# Open Source Project by Nevion Soft

Простое мобильное приложение, призваное помогать тем, кто забывает, закрыл ли дверь по выходу из дома<br>
Всё просто, закрыл дверь - поставил метку в приложение

<div align="center" style="display: flex; align-items: center;">
  <img src="./src/image/preview.jpg" alt="App preview" width="300" height="200">
</div>

# Стек технологий

<div align="center" style="display: flex; align-items: center;">
  <img src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" alt="React Native Logo" width="100" height="100">
  <span style="margin: 0 10px; font-size: 24px;"> </span>
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1280px-Unofficial_JavaScript_logo_2.svg.png" alt="Python Logo" width="100" height="100">
</div>

# Установка из исходников

```bash
git clone https://gitlab.com/web4450122/reminder-close-door.git

cd reminder-close-door

npm install
```
# Запуск на андроид эмуляторе 

```bash
npx react-native run-android
```
Если есть проблемы с запуском gradle

```bash
# cd android
gradlew clean
```

# Запуск на реальном андроид устройстве

```bash
npm run android
```

# Установка апк файла

Зайти в раздел [релиз](https://gitlab.com/web4450122/reminder-close-door/-/releases) и скачать файл CloseDoor.apk

# ССЫЛКИ

  [<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_tg.png?ref_type=heads" width="100">](https://t.me/ancient_nevionn)
  [<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_coffee.png?ref_type=heads" width="100">](https://www.donationalerts.com/r/nevion)
