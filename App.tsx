import React from 'react';
import MainMenu from './src/pages/MainMenu';

export const COLOR = {
  MAIN_COLOR: '#1c1549',
  BUTTON_COLOR: '#6934d1',
  SECONDARY_COLOR: '#2a206c',
};

export const APP_VERSION = '1.0.2';

const App = () => {
  return (
    <>
      <MainMenu />
    </>
  );
};

export default App;
