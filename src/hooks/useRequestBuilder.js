import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({name: 'database.db', location: 'default'});

db.transaction(tx => {
  tx.executeSql(
    'CREATE TABLE IF NOT EXISTS RemindClose (id INTEGER PRIMARY KEY AUTOINCREMENT, time TEXT)',
    [],
    (tx, results) => {
      console.log('Table created successfully');
    },
  );
});

const useAddCurrentDateToTable = () => {
  return date => {
    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO RemindClose (time) VALUES (?)',
        [date.toLocaleString()],
        (_, results) => {
          console.log('Переменная currentDate успешно добавлена в таблицу.');
        },
        error => {
          console.error('Ошибка при выполнении SQL-запроса:', error);
        },
      );
    });
  };
};

const useClearTable = () => {
  return () => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM RemindClose',
        [],
        () => {
          console.log('Все записи успешно удалены из таблицы.');
        },
        error => {
          console.error('Ошибка при удалении записей из таблицы:', error);
        },
      );
    });
  };
};

const useDeleteReminder = () => {
  return (index, callback) => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM RemindClose WHERE id = ?',
        [index],
        () => {
          console.log('Запись успешно удалена из таблицы.');
          if (callback) callback();
        },
        error => {
          console.error('Ошибка при удалении записи из таблицы:', error);
        },
      );
    });
  };
};

const useGetReminderData = () => {
  return setter => {
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM RemindClose',
        [],
        (_, {rows}) => {
          const data = [];
          for (let i = 0; i < rows.length; i++) {
            data.push({
              id: rows.item(i).id,
              time: rows.item(i).time,
            });
          }
          setter(data);
          console.log(data);
        },
        error => {
          console.error('Ошибка при выполнении SQL-запроса:', error);
        },
      );
    });
  };
};

export const useShowTableContent = () => {
  return () => {
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM RemindClose',
        [],
        (tx, result) => {
          if (result.rows.length > 0) {
            for (let i = 0; i < result.rows.length; i++) {
              const row = result.rows.item(i);
              console.log('Row:', row);
            }
          } else {
            console.log('Table is empty');
          }
        },
        error => {
          console.log('Error fetching table content:', error);
        },
      );
    });
  };
};

export function useSqlBuilder() {
  const saveReminder = useAddCurrentDateToTable();
  const updateData = useGetReminderData();
  const deleteCard = useDeleteReminder();
  const deleteAll = useClearTable();
  const check = useShowTableContent();

  return {
    saveReminder,
    updateData,
    deleteCard,
    deleteAll,
    check,
  };
}
