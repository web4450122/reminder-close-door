import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  StatusBar,
} from 'react-native';
import Cbutton from '../components/ui/Cbutton';
import ModalAbout from '../components/ui/ModalAbout';
import SvgLock from '../components/ui/icon/SvgLock';
import SvgAbout from '../components/ui/icon/SvgAbout';
import {useSqlBuilder} from '../hooks/useRequestBuilder';
import {COLOR} from '../../App';

const MainMenu = () => {
  const currentDate = new Date();

  const [reminderData, setReminderData] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const {saveReminder, updateData, deleteCard, deleteAll, check} =
    useSqlBuilder();

  useEffect(() => {
    updateData(setReminderData);
  }, []);

  function openModalAbout() {
    setIsModalVisible(true);
  }

  function closeModalAbout() {
    setIsModalVisible(false);
  }

  return (
    <View style={{...styles.container, backgroundColor: COLOR.SECONDARY_COLOR}}>
      <View style={styles.itemButtons}>
        <Cbutton
          styleButton={styles.buttonAdd}
          colorButton={{backgroundColor: COLOR.BUTTON_COLOR}}
          isShadow={true}
          isVisible={true}
          name={'Добавить'}
          onPress={() => {
            saveReminder(currentDate), updateData(setReminderData);
          }}
        />
        <Cbutton
          styleButton={styles.buttonDeleteAll}
          colorButton={{backgroundColor: COLOR.BUTTON_COLOR}}
          isShadow={true}
          isVisible={reminderData.length !== 0}
          name={'Удалить всё'}
          onPress={() => {
            deleteAll(), updateData(setReminderData);
          }}
        />
      </View>
      <TouchableOpacity
        onPress={() => openModalAbout()}
        style={styles.itemButtonSettings}>
        <SvgAbout />
      </TouchableOpacity>

      <View
        style={{...styles.insulatingItem, backgroundColor: COLOR.MAIN_COLOR}}>
        <StatusBar backgroundColor={COLOR.MAIN_COLOR} />

        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollView}>
          {reminderData.reverse().map((remind, index) => (
            <View
              style={{
                ...styles.cardInformation,
                backgroundColor: COLOR.SECONDARY_COLOR,
              }}
              key={reminderData.length - index}>
              <Text style={styles.text}>{remind.time}</Text>
              <View style={styles.itemMark}>
                <View style={styles.readyMark}>
                  <SvgLock />
                </View>

                <TouchableOpacity
                  onPress={() => {
                    deleteCard(remind.id), updateData(setReminderData);
                  }}>
                  <View style={styles.deleteButton}>
                    <Text style={styles.textCross}>X</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ))}
        </ScrollView>
        <ModalAbout isVisible={isModalVisible} onClose={closeModalAbout} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    borderWidth: 0,
    borderColor: 'transparent',
  },
  insulatingItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
    borderWidth: 0,
    borderColor: 'transparent',
  },
  scrollView: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: -10,
    minWidth: '100%',
    flexGrow: 1,
  },
  cardInformation: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 6,
    height: 50,
    borderRadius: 9,
    width: '80%',
    margin: 3,
  },
  itemMark: {
    width: 70,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  readyMark: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: '#03fc7f',
  },
  deleteButton: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: 'white',
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
  textCross: {
    fontSize: 20,
    color: 'black',
    textAlign: 'center',
  },
  itemButtons: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 12,
  },
  itemButtonSettings: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 9,
    right: 14,
  },
  buttonAdd: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
  buttonDeleteAll: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
});

export default MainMenu;
