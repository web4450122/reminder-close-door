import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgLock = () => (
  <Svg fill="none" height={24} width={20} marginLeft={3} marginTop={2}>
    <Path fill="#000" d="M12 15a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z" />
    <Path
      fill="#000"
      fillRule="evenodd"
      d="M19.695 14.353A5.002 5.002 0 0 0 16.5 9.33V6a4 4 0 0 0-4-4h-1a4 4 0 0 0-4 4v3.33a5.002 5.002 0 0 0-3.195 5.023l.212 3A5 5 0 0 0 9.505 22h4.99a5 5 0 0 0 4.988-4.647l.212-3ZM14.5 6v3h-5V6a2 2 0 0 1 2-2h1a2 2 0 0 1 2 2Zm-5.207 5A3 3 0 0 0 6.3 14.212l.212 3A3 3 0 0 0 9.505 20h4.99a3 3 0 0 0 2.993-2.788l.212-3A3 3 0 0 0 14.707 11H9.293Z"
      clipRule="evenodd"
    />
  </Svg>
);
export default SvgLock;
